import { Component } from '@angular/core';
import { Hero } from './hero';
//import { HeroDetailComponent } from './hero-detail.component';
import { HeroService } from './hero.service'
import { OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  moduleId: module.id,
  selector: 'my-heroes',
  templateUrl: '../../heroes.component.html',
  styleUrls: [ '../../heroes.component.css' ]
})

export class HeroesComponent implements OnInit{
  heroes: Hero[];
  //title = 'Tour of Heroes';
  selectedHero: Hero;


  // sin inyeccion de dependecias
  //heroService = new HeroService(); 
  //heroes = this.heroService.getHeroes();

  //con inyeccion de depencias
  constructor(
    private heroService: HeroService,
    private router: Router) {
    //no asigno la variable en el contructor porque es asincrono
    //this.heroes = this.heroService.getHeroes();
  }

  
  ngOnInit(): void {
    this.getHeroes();
  }

  
  getHeroes(): void {
    //asincronico
	  this.heroService.getHeroes().then(heroes => this.heroes = heroes);
    
    //sincronico
    //this.heroes = this.heroService.getHeroes();
  }
  // fin de la inyeccion

  onSelect(hero: Hero): void {
    this.selectedHero = hero;
  }
  
  gotoDetail(hero: Hero): void {
  	this.router.navigate(['/detail', this.selectedHero.id]);
  }

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.heroService.create(name)
      .then(hero => {
        this.heroes.push(hero);
        this.selectedHero = null;
      });
  }
  delete(hero: Hero): void {
    this.heroService
        .delete(hero.id)
        .then(() => {
          this.heroes = this.heroes.filter(h => h !== hero);
          if (this.selectedHero === hero) { this.selectedHero = null; }
        });
  }
}
//continua tutorial heroes
//https://angular.io/docs/ts/latest/tutorial/toh-pt5.html

